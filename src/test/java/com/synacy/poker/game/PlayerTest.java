package com.synacy.poker.game;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class PlayerTest {

	@Test
	public void getName() {
		Player player = new Player("Name");
		assertEquals("Name", player.getName());
	}

}