package com.synacy.poker.util;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.List;

import org.junit.Test;

import com.synacy.poker.card.Card;
import com.synacy.poker.card.CardRank;
import com.synacy.poker.card.CardSuit;

public class HandUtilityTest {
	  @Test
	    public void sortArrayListAscending_byRank() {
	        List<Card> cards = Arrays.asList(
	        		new Card(CardRank.KING, CardSuit.CLUBS),
	                new Card(CardRank.ACE, CardSuit.SPADES),
	                new Card(CardRank.THREE, CardSuit.CLUBS),
	                new Card(CardRank.TWO, CardSuit.HEARTS),
	                new Card(CardRank.QUEEN, CardSuit.CLUBS)
	        );

	        CardRank.ACE.setRankValue(1);
	        HandUtility.sortArrayListRankAscending(cards);
	        assertEquals("[A&spades;, 2&hearts;, 3&clubs;, Q&clubs;, K&clubs;]", cards.toString());
	    }

	  @Test
	    public void sortArrayListAscendingWithAceHigh_byRank() {
	        List<Card> cards = Arrays.asList(
	        		new Card(CardRank.JACK, CardSuit.CLUBS),
	                new Card(CardRank.ACE, CardSuit.HEARTS),
	                new Card(CardRank.SEVEN, CardSuit.CLUBS),
	                new Card(CardRank.TWO, CardSuit.CLUBS),
	                new Card(CardRank.QUEEN, CardSuit.DIAMONDS)
	        );
	        
	        HandUtility.sortArrayListRankAscendingWithAceHigh(cards);
	        assertEquals("[2&clubs;, 7&clubs;, J&clubs;, Q&diams;, A&hearts;]", cards.toString());
	    }
	  
	  @Test
	    public void sortArrayListAscending_bySuit() {
	        List<Card> cards = Arrays.asList(
	        		new Card(CardRank.JACK, CardSuit.CLUBS),
	                new Card(CardRank.ACE, CardSuit.HEARTS),
	                new Card(CardRank.SEVEN, CardSuit.SPADES),
	                new Card(CardRank.TWO, CardSuit.CLUBS),
	                new Card(CardRank.QUEEN, CardSuit.DIAMONDS)
	        );
	        
	        HandUtility.sortArrayListRankAscendingWithAceHigh(cards);
	        assertEquals("[2&clubs;, 7&spades;, J&clubs;, Q&diams;, A&hearts;]", cards.toString());
	    }
	  @Test
	    public void sortArrayListDescending_byRank() {
	        List<Card> cards = Arrays.asList(
	        		new Card(CardRank.KING, CardSuit.CLUBS),
	                new Card(CardRank.ACE, CardSuit.SPADES),
	                new Card(CardRank.THREE, CardSuit.CLUBS),
	                new Card(CardRank.TWO, CardSuit.HEARTS),
	                new Card(CardRank.QUEEN, CardSuit.CLUBS)
	        );

	        CardRank.ACE.setRankValue(1);
	        HandUtility.sortArrayListRankDescending(cards);
	        assertEquals("[K&clubs;, Q&clubs;, 3&clubs;, 2&hearts;, A&spades;]", cards.toString());
	    }

	  @Test
	    public void sortArrayListDescendingWithAceHigh_byRank() {
	        List<Card> cards = Arrays.asList(
	        		new Card(CardRank.JACK, CardSuit.CLUBS),
	                new Card(CardRank.ACE, CardSuit.HEARTS),
	                new Card(CardRank.SEVEN, CardSuit.CLUBS),
	                new Card(CardRank.TWO, CardSuit.CLUBS),
	                new Card(CardRank.QUEEN, CardSuit.DIAMONDS)
	        );
	        
	        HandUtility.sortArrayListRankDescendingWithAceHigh(cards);
	        assertEquals("[A&hearts;, Q&diams;, J&clubs;, 7&clubs;, 2&clubs;]", cards.toString());
	    }
	  
	  @Test
	    public void setAceToHighAce() {
	        List<Card> cards = Arrays.asList(
	        		new Card(CardRank.ACE, CardSuit.CLUBS),
	                new Card(CardRank.ACE, CardSuit.HEARTS),
	                new Card(CardRank.ACE, CardSuit.CLUBS),
	                new Card(CardRank.ACE, CardSuit.SPADES),
	                new Card(CardRank.ACE, CardSuit.DIAMONDS)
	        );
	        
	        HandUtility.sortArrayListRankDescendingWithAceHigh(cards);
	        for(Card card : cards) {
	        	assertEquals(card.getRank().getRankValue(),14);
	        }
	    }
}
