package com.synacy.poker.util;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import com.synacy.poker.card.Card;

/**
 * Utility methods for Hand module
 * 
 *
 */
public class HandUtility {

	/**
	 * Sort hand from lowest to highest based on rank value
	 * 
	 * @param hand
	 */
	public static void sortArrayListRankAscending(List<Card> hand) {
		// Sorts ArrayLists by the rank value of a Card
		Collections.sort(hand, new Comparator<Card>() {
			@Override
			public int compare(Card card1, Card card2) {
				return card1.getRank().getRankValue() - card2.getRank().getRankValue();
			}
		});
	}

	/**
	 * Sort hand from lowest to highest based on rank value
	 * 
	 * @param hand
	 */
	public static void sortArrayListRankAscendingWithAceHigh(List<Card> hand) {
		transformAceToHighAce(hand);
		// Sorts ArrayLists by the rank value of a Card
		Collections.sort(hand, new Comparator<Card>() {
			@Override
			public int compare(Card card1, Card card2) {
				return card1.getRank().getRankValue() - card2.getRank().getRankValue();
			}
		});
	}
	
	/**
	 * Sort hand from highest to lowest based on rank value
	 * 
	 * @param hand
	 */
	public static  void sortArrayListRankDescending(List<Card> hand) {
		// Sorts ArrayLists by the rank value of a Card
		Collections.sort(hand, new Comparator<Card>() {
			@Override
			public int compare(Card card1, Card card2) {
				return card2.getRank().getRankValue() - card1.getRank().getRankValue();
			}
		});
	}
	
	/**
	 * Sort hand from highest to lowest based on rank value
	 * 
	 * @param hand
	 */
	public static  void sortArrayListRankDescendingWithAceHigh(List<Card> hand) {
		transformAceToHighAce(hand);
		// Sorts ArrayLists by the rank value of a Card
		Collections.sort(hand, new Comparator<Card>() {
			@Override
			public int compare(Card card1, Card card2) {
				return card2.getRank().getRankValue() - card1.getRank().getRankValue();
			}
		});
	}
	
	/**
	 * Sort hand from based on order of the suit
	 * 
	 * @param hand
	 */
	public static  void sortArrayListSuitAscending(List<Card> hand) {
		Collections.sort(hand, new Comparator<Card>() {
			@Override
			public int compare(Card card1, Card card2) {
				return card1.getSuit().getColorValue() - card2.getSuit().getColorValue();
			}
		});
	}
	
	/**
	 * Set all Ace to Rank Value 14
	 * 
	 * @param hand
	 */
	public static void transformAceToHighAce(List<Card> hand) {
		// Finds Aces and replace rank value from 1 to 14
		for (Card card : hand) {
			if (card.getRank().getRankValue() == 1) {
				// Modifies Ace value to 14
				card.getRank().setRankValue(14);
			}
		}
	}
}
