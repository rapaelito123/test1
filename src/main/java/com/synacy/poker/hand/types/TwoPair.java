package com.synacy.poker.hand.types;

import java.util.List;

import com.synacy.poker.card.Card;
import com.synacy.poker.hand.Hand;
import com.synacy.poker.hand.HandType;

/**
 * @see <a href=
 *      "https://en.wikipedia.org/wiki/List_of_poker_hands#Two_pair">What is a
 *      Two Pair?</a>
 */
public class TwoPair extends Hand {

	private List<Card> firstPairCards;
	private List<Card> secondPairCards;
	private List<Card> otherCards;

	public TwoPair(List<Card> firstPairCards, List<Card> secondPairCards, List<Card> otherCards) {
		this.firstPairCards = firstPairCards;
		this.secondPairCards = secondPairCards;
		this.otherCards = otherCards;
	}

	public List<Card> getFirstPairCards() {
		return firstPairCards;
	}

	public List<Card> getSecondPairCards() {
		return secondPairCards;
	}

	public List<Card> getOtherCards() {
		return otherCards;
	}

	public HandType getHandType() {
		return HandType.TWO_PAIR;
	}

	/**
	 * @return The name of the hand with kicker ranked in descending order, e.g. Two
	 *         Pair (4,3) - A High
	 */
	@Override
	public String toString() {
		StringBuilder stringBuilder = new StringBuilder(new String());
		stringBuilder.append("Two Pair (");
		stringBuilder.append(firstPairCards.get(0).getRank().toString());
		stringBuilder.append(",");
		stringBuilder.append(secondPairCards.get(0).getRank().toString());
		stringBuilder.append(") - ");
		stringBuilder.append(otherCards.get(0).getRank().toString());
		stringBuilder.append(" High");

		return stringBuilder.toString();
	}

	@Override
	public int compareTo(Hand o) {
		if (getHandType().ordinal() > o.getHandType().ordinal())
			return 1;
		else if (getHandType().ordinal() < o.getHandType().ordinal())
			return -1;
		else {
			// Processes in this block would mean HandTypes are the same
			List<Card> tempCards = ((TwoPair) o).getFirstPairCards();
			if (firstPairCards.get(0).getRank().getRankValue() > tempCards.get(0).getRank().getRankValue())
				return 1;
			else if (firstPairCards.get(0).getRank().getRankValue() < tempCards.get(0).getRank().getRankValue())
				return -1;
			else {
				tempCards = ((TwoPair) o).getSecondPairCards();
				if (secondPairCards.get(0).getRank().getRankValue() > tempCards.get(0).getRank().getRankValue())
					return 1;
				else if (firstPairCards.get(0).getRank().getRankValue() < tempCards.get(0).getRank().getRankValue())
					return -1;
				else {
					// Processes in this block would mean HandTypes are the same
					tempCards = ((TwoPair) o).getOtherCards();
					if (otherCards.get(0).getRank().getRankValue() > tempCards.get(0).getRank().getRankValue())
						return 1;
					else if (otherCards.get(0).getRank().getRankValue() < tempCards.get(0).getRank().getRankValue())
						return -1;

				}
			}
		}
		return 0;
	}

	@Override
	public boolean equals(Object o) {
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        if(this.compareTo((TwoPair)o)==0)
        	return true; 
        return false;
	}
}
