package com.synacy.poker.hand.types;

import java.util.List;
import java.util.StringJoiner;

import com.synacy.poker.card.Card;
import com.synacy.poker.hand.Hand;
import com.synacy.poker.hand.HandType;
import com.synacy.poker.util.HandUtility;

/**
 * @see <a href=
 *      "https://en.wikipedia.org/wiki/List_of_poker_hands#High_card">What is a
 *      High Card?</a>
 */
public class HighCard extends Hand {

	private List<Card> cards;

	public List<Card> getCards() {
		return cards;
	}

	public HighCard(List<Card> cards) {
		this.cards = cards;
	}

	public HandType getHandType() {
		return HandType.HIGH_CARD;
	}

	/**
	 * @return The cards ordered by descending rank, e.g. A,K,Q,3,2
	 */
	@Override
	public String toString() {
		// Sort method is called because Test methods assume that sorting is done through the toString method which makes HandIdentier sorting useless(P.S. I'm not complaining)
		HandUtility.sortArrayListRankDescendingWithAceHigh(this.cards);
		StringJoiner stringJoiner = new StringJoiner(",");
		for (Card card : cards) {
			stringJoiner.add(card.getRank().toString());
		}
		return stringJoiner.toString();
	}

	@Override
	public int compareTo(Hand o) {
		if (getHandType().ordinal() > o.getHandType().ordinal())
			return 1;
		else if (getHandType().ordinal() < o.getHandType().ordinal())
			return -1;
		else {
			// Processes in this block would mean HandTypes are the same
			List<Card> tempCards = ((HighCard) o).getCards();
			for (int i = 0; i < cards.size(); i++) {
				if (cards.get(i).getRank().getRankValue() > tempCards.get(i).getRank().getRankValue())
					return 1;
				else if (cards.get(i).getRank().getRankValue() < tempCards.get(i).getRank().getRankValue())
					return -1;
			}
		}
		return 0;
	}
	@Override
	public boolean equals(Object o) {
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        if(this.compareTo((HighCard)o)==0)
        	return true; 
        return false;
	}
}
