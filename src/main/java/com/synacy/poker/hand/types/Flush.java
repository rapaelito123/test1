package com.synacy.poker.hand.types;

import java.util.List;

import com.synacy.poker.card.Card;
import com.synacy.poker.hand.Hand;
import com.synacy.poker.hand.HandType;
import com.synacy.poker.util.HandUtility;

/**
 * @see <a href="https://en.wikipedia.org/wiki/List_of_poker_hands#Flush">What is a flush?</a>
 */
public class Flush extends Hand {

    private List<Card> cards;

    public Flush(List<Card> cards) {
        this.cards = cards;
    }

    public HandType getHandType() {
        return HandType.FLUSH;
    }

    public List<Card> getCards() {
        return cards;
    }

	@Override
	public int compareTo(Hand o) {
		if(getHandType().ordinal()>o.getHandType().ordinal())
			return 1;
		else if(getHandType().ordinal()<o.getHandType().ordinal())
			return -1;
		else {
			//Processes in this block would mean HandTypes are the same
			List<Card> tempCards = ((Flush) o).getCards();
			for(int i=0; i < cards.size() ; i++ ) {
				if(cards.get(i).getRank().getRankValue()>tempCards.get(i).getRank().getRankValue())
					return 1;
				else if(cards.get(i).getRank().getRankValue()<tempCards.get(i).getRank().getRankValue())
					return -1;
			}
		}
		return 0;
	}
	
    /**
     * @return Returns the name of the hand and the highest card, e.g. Flush (K High)
     */
    @Override
    public String toString() {
    	StringBuilder stringBuilder = new StringBuilder(new String());
    	// Sort method is called because Test methods assume that sorting is done through the toString method which makes HandIdentier sorting useless(P.S. I'm not complaining)
    	HandUtility.sortArrayListRankDescendingWithAceHigh(this.cards);
    	stringBuilder.append("Flush (");
    	stringBuilder.append(this.cards.get(0).getRank().toString());
    	stringBuilder.append(" High)");
    	return stringBuilder.toString();
    }

    @Override
	public boolean equals(Object o) {
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        if(this.compareTo((Flush)o)==0)
        	return true; 
        return false;
	}
}
