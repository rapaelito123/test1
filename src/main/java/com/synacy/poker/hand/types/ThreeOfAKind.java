package com.synacy.poker.hand.types;

import java.util.List;
import java.util.StringJoiner;

import com.synacy.poker.card.Card;
import com.synacy.poker.hand.Hand;
import com.synacy.poker.hand.HandType;
import com.synacy.poker.util.HandUtility;

/**
 * @see <a href="https://en.wikipedia.org/wiki/List_of_poker_hands#Three_of_a_kind">What is a Three of a Kind?</a>
 */
public class ThreeOfAKind extends Hand {

    private List<Card> threeOfAKindCards;
    private List<Card> otherCards;
    public HandType getHandType() {
        return HandType.THREE_OF_A_KIND;
    }

    
    public List<Card> getThreeOfAKindCards() {
    	return threeOfAKindCards;
    }
    
    public List<Card> getOtherCards() {
    	return otherCards;
    }
    
    public ThreeOfAKind(List<Card> threeOfAKindCards, List<Card> otherCards) {
    	this.threeOfAKindCards = threeOfAKindCards;
    	this.otherCards = otherCards;
    }
    
    /**
     * @return The name of the hand plus kickers in descending rank, e.g. Trips (4) - A,2 High
     */
    @Override
    public String toString() {
    	StringBuilder stringBuilder = new StringBuilder(new String());
    	StringJoiner stringJoiner = new StringJoiner(",");
    	// Sort method is called because Test methods assume that sorting is done through the toString method which makes HandIdentier sorting useless(P.S. I'm not complaining)
    	HandUtility.sortArrayListRankDescendingWithAceHigh(this.otherCards);
    	for(Card card: otherCards) {
    		stringJoiner.add(card.getRank().toString());
    	}
    	stringBuilder.append("Trips (");
    	stringBuilder.append(threeOfAKindCards.get(0).getRank().toString());
    	stringBuilder.append(") - ");
        if(stringJoiner.toString().length()>0) {
        	stringBuilder.append(stringJoiner.toString());
        	stringBuilder.append(" High");
        }
        return stringBuilder.toString();
    }
    
	@Override
	public int compareTo(Hand o) {
		if (getHandType().ordinal() > o.getHandType().ordinal())
			return 1;
		else if (getHandType().ordinal() < o.getHandType().ordinal())
			return -1;
		else {
			List<Card> tempCards = ((ThreeOfAKind) o).getThreeOfAKindCards();
			if (threeOfAKindCards.get(0).getRank().getRankValue() > tempCards.get(0).getRank().getRankValue())
				return 1;
			else if (threeOfAKindCards.get(0).getRank().getRankValue() < tempCards.get(0).getRank().getRankValue())
				return -1;
			else {
				// Processes in this block would mean HandTypes are the same
				tempCards = ((ThreeOfAKind) o).getOtherCards();
				for (int i = 0; i < otherCards.size(); i++) {
					if (otherCards.get(i).getRank().getRankValue() > tempCards.get(i).getRank().getRankValue())
						return 1;
					else if (otherCards.get(i).getRank().getRankValue() < tempCards.get(i).getRank().getRankValue())
						return -1;
				}
			}
		}
		return 0;
	}

	@Override
	public boolean equals(Object o) {
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        if(this.compareTo((ThreeOfAKind)o)==0)
        	return true; 
        return false;
	}

}
