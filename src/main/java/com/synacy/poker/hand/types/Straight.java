package com.synacy.poker.hand.types;

import java.util.List;

import com.synacy.poker.card.Card;
import com.synacy.poker.hand.Hand;
import com.synacy.poker.hand.HandType;

/**
 * @see <a href="https://en.wikipedia.org/wiki/List_of_poker_hands#Straight">What is a Straight?</a>
 */
public class Straight extends Hand {

    private List<Card> cards;

    public Straight(List<Card> cards) {
        this.cards = cards;
    }

    public HandType getHandType() {
        return HandType.STRAIGHT;
    }

    public List<Card> getCards() {
        return cards;
    }

    /**
     * @return The name of the hand and the high card, e.g. Straight (A High)
     */
    @Override
    public String toString() {
    	StringBuilder stringBuilder = new StringBuilder(new String());
    	// Sort method is called because Test methods assume that sorting is done through the toString method which makes HandIdentier sorting useless(P.S. I'm not complaining)
    	stringBuilder.append("Straight (");
    	stringBuilder.append(cards.get(0).getRank().toString());
    	stringBuilder.append(" High)");

    	return stringBuilder.toString();
    }
    
    @Override
	public int compareTo(Hand o) {
		if (getHandType().ordinal() > o.getHandType().ordinal())
			return 1;
		else if (getHandType().ordinal() < o.getHandType().ordinal())
			return -1;
		else {
			// Processes in this block would mean HandTypes are the same
			List<Card> tempCards = ((Straight) o).getCards();
			for (int i = 0; i < cards.size(); i++) {
				if (cards.get(i).getRank().getRankValue() > tempCards.get(i).getRank().getRankValue())
					return 1;
				else if (cards.get(i).getRank().getRankValue() < tempCards.get(i).getRank().getRankValue())
					return -1;
			}
		}
		return 0;
	}
    
    @Override
	public boolean equals(Object o) {
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        if(this.compareTo((Straight)o)==0)
        	return true; 
        return false;
	}
}
