package com.synacy.poker.hand.types;

import java.util.List;
import java.util.StringJoiner;

import com.synacy.poker.card.Card;
import com.synacy.poker.hand.Hand;
import com.synacy.poker.hand.HandType;
import com.synacy.poker.util.HandUtility;

/**
 * @see <a href=
 *      "https://en.wikipedia.org/wiki/List_of_poker_hands#One_pair">What is a
 *      One Pair?</a>
 */
public class OnePair extends Hand {

	private List<Card> pairCards;
	private List<Card> otherCards;

	public OnePair(List<Card> pairCards, List<Card> otherCards) {
		this.pairCards = pairCards;
		this.otherCards = otherCards;
	}

	public List<Card> getPairCards() {
		return pairCards;
	}

	public List<Card> getOtherCards() {
		return otherCards;
	}

	public HandType getHandType() {
		return HandType.ONE_PAIR;
	}

	/**
	 * @return The name of the hand plus kickers ordered by descending rank, e.g.
	 *         One Pair (2) - A,K,Q High, or the name of the hand and rank if there
	 *         are no community cards yet in play, e.g. One Pair (2)
	 */
	@Override
	public String toString() {
		StringBuilder stringBuilder = new StringBuilder(new String());
		StringJoiner stringJoiner = new StringJoiner(",");
		// Sort method is called because Test methods assume that sorting is done through the toString method which makes HandIdentier sorting useless(P.S. I'm not complaining)
		HandUtility.sortArrayListRankDescendingWithAceHigh(this.otherCards);
		for (Card card : otherCards) {
			stringJoiner.add(card.getRank().toString());
		}
		stringBuilder.append("One Pair (");
		stringBuilder.append(pairCards.get(0).getRank().toString());
		stringBuilder.append(")");
		if (stringJoiner.toString().length() > 0) {
			stringBuilder.append(" - ");
			stringBuilder.append(stringJoiner.toString());
			stringBuilder.append(" High");
		}
		return stringBuilder.toString();
	}

	@Override
	public int compareTo(Hand o) {
		if (getHandType().ordinal() > o.getHandType().ordinal())
			return 1;
		else if (getHandType().ordinal() < o.getHandType().ordinal())
			return -1;
		else {
			List<Card> tempCards = ((OnePair) o).getPairCards();
			if (pairCards.get(0).getRank().getRankValue() > tempCards.get(0).getRank().getRankValue())
				return 1;
			else if (pairCards.get(0).getRank().getRankValue() < tempCards.get(0).getRank().getRankValue())
				return -1;
			else {
				// Processes in this block would mean HandTypes are the same
				tempCards = ((OnePair) o).getOtherCards();
				for (int i = 0; i < otherCards.size(); i++) {
					if (otherCards.get(i).getRank().getRankValue() > tempCards.get(i).getRank().getRankValue())
						return 1;
					else if (otherCards.get(i).getRank().getRankValue() < tempCards.get(i).getRank().getRankValue())
						return -1;
				}
			}
		}
		return 0;
	}

	@Override
	public boolean equals(Object o) {
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        if(this.compareTo((OnePair)o)==0)
        	return true; 
        return false;
	}
}
