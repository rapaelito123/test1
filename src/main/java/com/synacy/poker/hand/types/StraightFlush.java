package com.synacy.poker.hand.types;

import java.util.List;

import com.synacy.poker.card.Card;
import com.synacy.poker.card.CardRank;
import com.synacy.poker.hand.HandType;

/**
 * @see <a href=
 *      "https://en.wikipedia.org/wiki/List_of_poker_hands#Straight_flush">What
 *      is a Straight Flush?</a>
 */
public class StraightFlush extends Straight {

	public StraightFlush(List<Card> cards) {
		super(cards);
	}

	@Override
	public HandType getHandType() {
		return HandType.STRAIGHT_FLUSH;
	}

	/**
	 * @return Royal Flush if the hand is a royal flush, or Straight Flush with the
	 *         highest rank card, e.g. Straight Flush (K High)
	 */
	@Override
	public String toString() {
		StringBuilder stringBuilder = new StringBuilder(new String());
		// Sort method is called because Test methods assume that sorting is done through the toString method which makes HandIdentier sorting useless(P.S. I'm not complaining)
		if (this.getCards().get(0).getRank().equals(CardRank.ACE)) {
			stringBuilder.append("Royal Flush");
		} else {
			stringBuilder.append("Straight Flush (");
			stringBuilder.append(this.getCards().get(0).getRank().toString());
			stringBuilder.append(" High)");
		}
		return stringBuilder.toString();
	}
	
	@Override
	public boolean equals(Object o) {
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        if(this.compareTo((StraightFlush)o)==0)
        	return true; 
        return false;
	}
}
