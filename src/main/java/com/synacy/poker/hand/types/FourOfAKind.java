package com.synacy.poker.hand.types;

import java.util.List;

import com.synacy.poker.card.Card;
import com.synacy.poker.hand.Hand;
import com.synacy.poker.hand.HandType;

/**
 * @see <a href=
 *      "https://en.wikipedia.org/wiki/List_of_poker_hands#Four_of_a_kind">What
 *      is a Four of a Kind?</a>
 */
public class FourOfAKind extends Hand {

	private List<Card> fourOfAKindCards;
	private List<Card> otherCards;

	public FourOfAKind(List<Card> fourOfAKindCards, List<Card> otherCards) {
		this.fourOfAKindCards = fourOfAKindCards;
		this.otherCards = otherCards;
	}

	public List<Card> getOtherCards() {
		return otherCards;
	}

	public void setOtherCards(List<Card> otherCards) {
		this.otherCards = otherCards;
	}

	public List<Card> getFourOfAKindCards() {
		return fourOfAKindCards;
	}

	public HandType getHandType() {
		return HandType.FOUR_OF_A_KIND;
	}

	@Override
	public int compareTo(Hand o) {
		if (getHandType().ordinal() > o.getHandType().ordinal())
			return 1;
		else if (getHandType().ordinal() < o.getHandType().ordinal())
			return -1;
		else {
			// Processes in this block would mean HandTypes are the same
			if (fourOfAKindCards.get(0).getRank().getRankValue() > ((FourOfAKind) o).getFourOfAKindCards().get(0)
					.getRank().getRankValue())
				return 1;
			else if (fourOfAKindCards.get(0).getRank().getRankValue() < ((FourOfAKind) o).getFourOfAKindCards().get(0)
					.getRank().getRankValue())
				return -1;
			else {
				if (otherCards.get(0).getRank().getRankValue() > ((FourOfAKind) o).getOtherCards().get(0).getRank()
						.getRankValue())
					return 1;
				else if (otherCards.get(0).getRank().getRankValue() < ((FourOfAKind) o).getOtherCards().get(0).getRank()
						.getRankValue())
					return -1;
			}
		}
		return 0;
	}

	/**
	 * @return Returns the name of the hand plus kicker, e.g. Quads (4) - A High
	 */
	@Override
	public String toString() {
		StringBuilder stringBuilder = new StringBuilder(new String());
		stringBuilder.append("Quads (");
		stringBuilder.append(fourOfAKindCards.get(0).getRank().toString());
		stringBuilder.append(") - ");
		stringBuilder.append(otherCards.get(0).getRank().toString());
		stringBuilder.append(" High");
		return stringBuilder.toString();
	}
	@Override
	public boolean equals(Object o) {
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        if(this.compareTo((FourOfAKind)o)==0)
        	return true; 
        return false;
	}
}
