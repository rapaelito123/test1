package com.synacy.poker.hand.types;

import java.util.List;

import com.synacy.poker.card.Card;
import com.synacy.poker.hand.Hand;
import com.synacy.poker.hand.HandType;

/**
 * @see <a href="https://en.wikipedia.org/wiki/List_of_poker_hands#Full_house">What is a Full House?</a>
 */
public class FullHouse extends Hand {

    private List<Card> threeOfAKindCards;
	private List<Card> pairCards;

    public FullHouse(List<Card> threeOfAKindCards, List<Card> pairCards) {
        this.threeOfAKindCards = threeOfAKindCards;
        this.pairCards = pairCards;
    }
 
    public List<Card> getThreeOfAKindCards() {
    	return threeOfAKindCards;
    }
    
    public List<Card> getPairCards() {
    	return pairCards;
    }
    public HandType getHandType() {
        return HandType.FULL_HOUSE;
    }

    /**
     * @return The name of the hand with rank of the three pair and two pair, e.g.
     * 444AA - Full House (4,A)
     */
    @Override
    public String toString() {
    	StringBuilder stringBuilder = new StringBuilder(new String());
    	stringBuilder.append("Full House (");
    	stringBuilder.append(threeOfAKindCards.get(0).getRank().toString());
    	stringBuilder.append(",");
    	stringBuilder.append(pairCards.get(0).getRank().toString());
    	stringBuilder.append(")");
    	
    	return stringBuilder.toString();
    }

    @Override
	public int compareTo(Hand o) {
		if (getHandType().ordinal() > o.getHandType().ordinal())
			return 1;
		else if (getHandType().ordinal() < o.getHandType().ordinal())
			return -1;
		else {
			// Processes in this block would mean HandTypes are the same
			if (threeOfAKindCards.get(0).getRank().getRankValue() > ((FullHouse) o).getThreeOfAKindCards().get(0)
					.getRank().getRankValue())
				return 1;
			else if (threeOfAKindCards.get(0).getRank().getRankValue() < ((FullHouse) o).getThreeOfAKindCards().get(0)
					.getRank().getRankValue())
				return -1;
			else {
				if (pairCards.get(0).getRank().getRankValue() > ((FullHouse) o).getPairCards().get(0).getRank()
						.getRankValue())
					return 1;
				else if (pairCards.get(0).getRank().getRankValue() < ((FullHouse) o).getPairCards().get(0).getRank()
						.getRankValue())
					return -1;
			}
		}
		return 0;
	}
    
    @Override
	public boolean equals(Object o) {
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        if(this.compareTo((FullHouse)o)==0)
        	return true; 
        return false;
	}
}
