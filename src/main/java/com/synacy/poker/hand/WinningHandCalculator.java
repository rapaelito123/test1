package com.synacy.poker.hand;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Component;

/**
 * A service class used to calculate the winning hand.
 */
@Component
public class WinningHandCalculator {

	/**
	 * @param playerHands
	 * @return The winning {@link Hand} from a list of player hands.
	 */
	public Optional<Hand> calculateWinningHand(List<Hand> playerHands) {
		
		for (int i = 0; i < playerHands.size(); i++) {
			if ((i + 1) < playerHands.size())
				for (int j = i + 1; j < playerHands.size(); j++) {
					if (playerHands.get(i).compareTo(playerHands.get(j)) > 0) {
						playerHands.remove(j);
						j--;
					} else if (playerHands.get(i).compareTo(playerHands.get(j)) < 0) {
						playerHands.remove(i);
						i--;
						break;
					}
				}
		}
		return Optional.of(playerHands.get(0));
	}
}
