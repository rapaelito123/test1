package com.synacy.poker.hand;

/**
 * An enum of {@link Hand}s
 *
 * <ul>
 * <li>High Card</li>
 * <li>One Pair</li>
 * <li>Two Pair</li>
 * <li>Three of a Kind</li>
 * <li>Straight</li>
 * <li>Flush</li>
 * <li>Full House</li>
 * <li>Four of a Kind</li>
 * <li>Straight Flush</li>
 * </ul>
 */
public enum HandType {
    HIGH_CARD("HIGH"),
    ONE_PAIR("One Pair"),
    TWO_PAIR("Two Pair"),
    THREE_OF_A_KIND("Three of a Kind"),
    STRAIGHT("Straight"),
    FLUSH("Flush"),
    FULL_HOUSE("Full House"),
    FOUR_OF_A_KIND("Four of a kind"),
    STRAIGHT_FLUSH("Straigh Flush");
    

	private String value;
	
    HandType(String value) {
    	this.value = value;
    }
    
    @Override
	public String toString() {
		return value;
	}
}

