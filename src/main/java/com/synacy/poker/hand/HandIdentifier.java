package com.synacy.poker.hand;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.synacy.poker.card.Card;
import com.synacy.poker.card.CardRank;
import com.synacy.poker.card.CardSuit;
import com.synacy.poker.hand.types.Flush;
import com.synacy.poker.hand.types.FourOfAKind;
import com.synacy.poker.hand.types.FullHouse;
import com.synacy.poker.hand.types.HighCard;
import com.synacy.poker.hand.types.OnePair;
import com.synacy.poker.hand.types.Straight;
import com.synacy.poker.hand.types.StraightFlush;
import com.synacy.poker.hand.types.ThreeOfAKind;
import com.synacy.poker.hand.types.TwoPair;
import com.synacy.poker.util.HandUtility;

/**
 * A service that is to used to identify the {@link Hand} given the player's
 * cards and the community cards.
 */
@Component
public class HandIdentifier {

	private static final int TWO_OR_FOUR_KIND_KICKER_LIMIT = 1;
	private static final int THREE_OF_A_KIND_KICKER_LIMIT = 2;
	private static final int MAX_HAND = 5;
	private static final int ONE_PAIR_KICKER_LIMIT = 3;
	private int flushSuit;
	private boolean isLowAce;
	private int rankHolder1;
	private int rankHolder2;

	private void initialize() {
		this.flushSuit = 0;
		this.rankHolder1 = 0;
		this.rankHolder2 = 0;
		this.isLowAce = false;
	}

	/**
	 * Given the player's cards and the community cards, identifies the player's
	 * hand.
	 *
	 * @param playerCards
	 * @param communityCards
	 * @return The player's {@link Hand} or `null` if no Hand was identified.
	 */
	public Hand identifyHand(List<Card> playerCards, List<Card> communityCards) {
		ArrayList<Card> hand = new ArrayList<>();
		HandType handType, tempHandType;
		for (Card card : playerCards)
			hand.add(card);
		for (Card card : communityCards)
			hand.add(card);

		// Initialize class variables
		initialize();

		// Convert Ace to Ace High and sorth Hand from lowest to highest based on rank
		// value
		HandUtility.sortArrayListRankAscendingWithAceHigh(hand);

		// Check if Hand would contain any of the following:
		// One Pair, Two Pair, Three of a Kind, or Full House
		handType = checkSets(playerCards, hand);

		// Check if Hand would contain any of the following:
		// Straight, Flush or Straight Flush
		tempHandType = checkStraightFlush(playerCards, hand);

		// Compare which HandType has a higher precedence
		if (tempHandType.ordinal() > handType.ordinal())
			handType = tempHandType;
		// Sort List of Cards from Highest to Lowest according to Rank value
		HandUtility.sortArrayListRankDescending(hand);
		return createHand(hand, handType);
	}

	/**
	 * Create appropriate hand given the player's cards and the community cards
	 * along with the identified highest HandType.
	 *
	 * @param hand
	 * @param handType
	 * @return The player's {@link Hand} or `null` if no Hand was identified.
	 */
	private Hand createHand(ArrayList<Card> hand, HandType handType) {
		Hand result = null;
		switch (handType) {
		case STRAIGHT_FLUSH:
			result = createStraightFlush(hand);
			break;
		case FOUR_OF_A_KIND:
			result = createThreeOrFourOfAKind(hand, HandType.FOUR_OF_A_KIND);
			break;
		case FULL_HOUSE:
			result = createFullHouse(hand);
			break;
		case FLUSH:
			result = createFlush(hand);
			break;
		case STRAIGHT:
			result = createStraight(hand);
			break;
		case THREE_OF_A_KIND:
			result = createThreeOrFourOfAKind(hand, HandType.THREE_OF_A_KIND);
			break;
		case TWO_PAIR:
			result = createTwoPair(hand);
			break;
		case ONE_PAIR:
			result = createOnePair(hand);
			break;
		case HIGH_CARD:
			result = createHighCard(hand);
			break;
		}

		return result;
	}

	/**
	 * Create OnePair Hand that contains a list of Pair cards and a list of Kicker
	 * cards
	 *
	 * @param hand
	 * @return The player's {@link OnePair} Hand.
	 */
	private Hand createOnePair(ArrayList<Card> hand) {
		ArrayList<Card> pairCards = new ArrayList<>();
		ArrayList<Card> otherCards = new ArrayList<>();
		int i = 0, limit = ONE_PAIR_KICKER_LIMIT;
		for (Card card : hand) {
			if (card.getRank().getRankValue() == rankHolder1)
				pairCards.add(card);
			else if (i++ < limit)
				otherCards.add(card);

		}
		return new OnePair(pairCards, otherCards);
	}

	/**
	 * Create a Three of a Kind Hand or a Four of a Kind hand that contains a list
	 * of matching cards and a list of Kicker card/s
	 *
	 * @param hand
	 * @param handType
	 * @return The player's {@link ThreeOfAKind} Hand or {@link FourOfAKind} Hand .
	 */
	private Hand createThreeOrFourOfAKind(ArrayList<Card> hand, HandType handType) {
		ArrayList<Card> cards = new ArrayList<>();
		ArrayList<Card> otherCards = new ArrayList<>();
		int i = 0, limit = handType.equals(HandType.THREE_OF_A_KIND) ? THREE_OF_A_KIND_KICKER_LIMIT
				: TWO_OR_FOUR_KIND_KICKER_LIMIT;

		for (Card card : hand) {
			if (card.getRank().getRankValue() == rankHolder1)
				cards.add(card);
			else if (i++ < limit)
				otherCards.add(card);
		}
		return handType.equals(HandType.THREE_OF_A_KIND) ? new ThreeOfAKind(cards, otherCards)
				: new FourOfAKind(cards, otherCards);
	}

	/**
	 * Create a Two Pair Hand containing the first pairing cards, second pairing
	 * cards and the kicker card
	 * 
	 * @param hand
	 * @return The player's {@link TwoPair} Hand
	 */
	private Hand createTwoPair(ArrayList<Card> hand) {
		ArrayList<Card> firstPair = new ArrayList<>(), secondPair = new ArrayList<>(), otherCards = new ArrayList<>();
		int i = 0, limit = 1;
		for (Card card : hand) {
			if (card.getRank().getRankValue() == rankHolder1)
				firstPair.add(card);
			else if (card.getRank().getRankValue() == rankHolder2)
				secondPair.add(card);
			else if (i++ < limit)
				otherCards.add(card);
		}
		return new TwoPair(firstPair, secondPair, otherCards);
	}

	/**
	 * Create a Full House Hand containing the three of a kind cards, and the pair
	 * cards
	 * 
	 * @param hand
	 * @return The player's {@link FullHouse} Hand
	 */
	private Hand createFullHouse(ArrayList<Card> hand) {
		ArrayList<Card> threeOfAKindCards = new ArrayList<>(), pairCards = new ArrayList<>();
		for (Card card : hand) {
			if (card.getRank().getRankValue() == rankHolder1)
				threeOfAKindCards.add(card);
			else if (card.getRank().getRankValue() == rankHolder2)
				pairCards.add(card);
		}
		return new FullHouse(threeOfAKindCards, pairCards);
	}

	/**
	 * Create a High Card hand containing the top 5 cards based on rank value
	 * 
	 * @param hand
	 * @return The player's {@link HighCard} Hand
	 */
	private Hand createHighCard(ArrayList<Card> hand) {
		ArrayList<Card> cards = new ArrayList<>();
		int i = 0;
		for (Card card : hand) {
			if (i++ < MAX_HAND) {
				cards.add(card);
			}
		}
		return new HighCard(cards);
	}

	/**
	 * Check whether HandType is either Flush, Straight or Straight Flush
	 * 
	 * @param playerCards
	 * @param hand
	 * @return HandType.FLUSH, HandType.STRAIGHT, HandType.STRAIGHT_FLUSH
	 */
	private HandType checkStraightFlush(List<Card> playerCards, ArrayList<Card> hand) {
		HandType handType;
		handType = checkFlush(playerCards, hand);

		if (checkStraight(playerCards, hand) == HandType.STRAIGHT
				|| checkLowAceStraight(playerCards, hand) == HandType.STRAIGHT) {
			handType = (handType.equals(HandType.FLUSH)) ? HandType.STRAIGHT_FLUSH : HandType.STRAIGHT;
		}
		return handType;
	}

	/**
	 * Check if Player's Hand contains the cards that have the same {@link CardSuit}
	 * 
	 * @param playerCards
	 * @param hand
	 * @return HandType.FLUSH or HandType.HIGH_CARD
	 */
	private HandType checkFlush(List<Card> playerCards, ArrayList<Card> hand) {
		HandUtility.sortArrayListSuitAscending(hand);

		int counter = 1, playerCard1, playerCard2;
		boolean isFlush = false;
		playerCard1 = playerCards.get(0).getSuit().getColorValue();
		playerCard2 = playerCards.get(1).getSuit().getColorValue();

		for (int i = 0; i < hand.size() - 1; i++) {
			// Compare if Card Suit is the same
			if (hand.get(i).getSuit().getColorValue() == hand.get(i + 1).getSuit().getColorValue()) {
				counter = counter + 1;
			} else {
				counter = 1;
			}
			// Store Suit once counter is equal to 5
			if (counter == MAX_HAND) {
				this.flushSuit = hand.get(i).getSuit().getColorValue();
				// Check if Player Card contains the same suit as the Flush suit
				if (playerCard1 == this.flushSuit || playerCard2 == this.flushSuit)
					isFlush = true;
			}
		}
		return (isFlush) ? HandType.FLUSH : HandType.HIGH_CARD;
	}

	/**
	 * Create Flush Hand containing the cards that have the same {@link CardSuit}
	 * 
	 * @param hand
	 * @return {@link Flush}
	 */
	private Hand createFlush(ArrayList<Card> hand) {
		List<Card> flushList = new ArrayList<>();
		int counter = 0;
		for (Card card : hand) {
			// Add Cards to list if Card Suit is the same as the stored Flush suit
			if (card.getSuit().getColorValue() == this.flushSuit) {
				flushList.add(card);
				counter++;
			}
			if (counter == MAX_HAND)
				break;
		}
		return new Flush(flushList);
	}

	/**
	 * Check if Player's Hand contains the cards that have a consecutive incremental
	 * {@link CardRank}
	 * 
	 * @param playerCards
	 * @param hand
	 * @return HandType.STRAIGHT or HandType.HIGH_CARD
	 */
	private HandType checkStraight(List<Card> playerCards, ArrayList<Card> hand) {

		int counter = 1, playerCard1, playerCard2;
		boolean isStraight = false;
		//
		playerCard1 = playerCards.get(0).getRank().getRankValue();
		playerCard2 = playerCards.get(1).getRank().getRankValue();
		HandUtility.sortArrayListRankAscending(hand);
		// Loop from max to 1 to keep track where the straight would start.
		for (int i = hand.size() - 1; i > 0; i--) {
			if (hand.get(i).getRank().getRankValue() == (hand.get(i - 1).getRank().getRankValue() + 1)) {
				counter = counter + 1;
			} else if (hand.get(i).getRank().getRankValue() == hand.get(i - 1).getRank().getRankValue()) {
				// Do nothing since there might be a possibility that same rank exists in hand
			} else {
				counter = 1;
			}
			if (counter == MAX_HAND) {
				int tempIndex = i - 1;
				if ((playerCard1 >= hand.get(tempIndex).getRank().getRankValue()
						&& playerCard1 <= hand.get(tempIndex + MAX_HAND - 1).getRank().getRankValue())
						|| (playerCard2 >= hand.get(tempIndex).getRank().getRankValue()
								&& playerCard2 <= hand.get(tempIndex + MAX_HAND - 1).getRank().getRankValue()))
					isStraight = true;
			}
		}

		if (isStraight) {
			return HandType.STRAIGHT;
		}
		return (isStraight) ? HandType.STRAIGHT : HandType.HIGH_CARD;
	}

	/**
	 * Check if Player's Hand contains the cards that have a consecutive incremental
	 * {@link CardRank} with Ace as a low rank card
	 * 
	 * @param playerCards
	 * @param hand
	 * @return HandType.STRAIGHT or HandType.HIGH_CARD
	 */
	private HandType checkLowAceStraight(List<Card> playerCards, ArrayList<Card> hand) {
		HandType handType = HandType.HIGH_CARD;
		Card aceCard;
		HandUtility.sortArrayListRankDescending(hand);
		aceCard = hand.get(0);
		// Check if Ace exists
		if (aceCard.getRank().getRankValue() == 14) {
			// Modifies Ace value to 1
			aceCard.getRank().setRankValue(1);
			// Re-sort the hand
			handType = checkStraight(playerCards, hand);
			aceCard.getRank().setRankValue(14);
		}
		// Set indicator if Ace Low Straight exists
		if (handType.equals(HandType.STRAIGHT))
			this.isLowAce = true;
		return handType;
	}

	/**
	 * Create a Hand that contains cards that have a consecutive incremental rank
	 * value {@link CardRank}
	 * 
	 * @param hand
	 * @return {@link Straight}
	 */
	private Hand createStraight(ArrayList<Card> hand) {
		ArrayList<Card> cards = new ArrayList<Card>();
		int counter = 1;

		if (isLowAce) {
			HandUtility.sortArrayListRankDescending(hand);
			if (hand.get(0).getRank().getRankValue() == 14)
				hand.get(0).getRank().setRankValue(1);
		} else
			HandUtility.sortArrayListRankDescendingWithAceHigh(hand);
		// Loop from 0 to size-1 to extract sublist start and end
		for (int i = 0; i < hand.size() - 1; i++) {

			Card currentCard = hand.get(i);
			Card nextCard = hand.get(i + 1);
			// Check if the current card's rank value is an increment to the next card
			if (currentCard.getRank().getRankValue() == (nextCard.getRank().getRankValue() + 1)) {
				counter = counter + 1;
				cards.add(currentCard);
			} else if (currentCard.getRank().getRankValue() == nextCard.getRank().getRankValue()) {
				// Do nothing since there might be a possibility that same rank exists in hand
			} else {
				cards.clear();
				counter = 1;
			}
			if (counter == MAX_HAND) {
				cards.add(nextCard);
				break;
			}
		}
		return new Straight(cards);
	}

	/**
	 * Create a Hand that contains cards that have a consecutive incremental
	 * {@link CardRank} and has the same {@link CardSuit}
	 * 
	 * @param hand
	 * @return {@link StraightFlush}
	 */
	private Hand createStraightFlush(ArrayList<Card> hand) {
		ArrayList<Card> cards = new ArrayList<Card>();
		int counter = 1;
		if (isLowAce) {
			HandUtility.sortArrayListRankDescending(hand);
			if (hand.get(0).getRank().getRankValue() == 14)
				hand.get(0).getRank().setRankValue(1);
		} else
			HandUtility.sortArrayListRankDescendingWithAceHigh(hand);

		// Loop from 0 to size-1 to extract sublist start and end
		for (int i = 0; i < hand.size() - 1; i++) {
			// Check if the current card's rank value is an increment to the next card
			Card currentCard = hand.get(i);
			Card nextCard = hand.get(i + 1);
			if (currentCard.getRank().getRankValue() == (nextCard.getRank().getRankValue() + 1)) {
				if (currentCard.getSuit().getColorValue() == this.flushSuit) {
					counter = counter + 1;
					cards.add(currentCard);
				}
			} else if (currentCard.getRank().getRankValue() == nextCard.getRank().getRankValue()) {
				if (currentCard.getSuit().getColorValue() == this.flushSuit) {
					counter = counter + 1;
					cards.add(currentCard);
				}
			} else {
				cards.clear();
				counter = 1;
			}
			if (counter == MAX_HAND) {
				if (nextCard.getSuit().getColorValue() == this.flushSuit)
					cards.add(nextCard);
				break;
			}
		}
		return new StraightFlush(cards);
	}

	/**
	 * Check if Player's Hand contains a Pair, Two Pair, Three of a Kind, Full House
	 * or Four of a Kind
	 * 
	 * @param playerCards
	 * @param hand
	 * @return HandType.ONE_PAIR/HandType.TWO_PAIR/HandType.THREE_OF_A_KIND/
	 *         HandType.FULL_HOUSE/HandType.FOUR_OF_A_KIND
	 */
	private HandType checkSets(List<Card> playerCards, ArrayList<Card> hand) {
		HandType handType = HandType.HIGH_CARD;
		int containerCount1 = 0, containerCount2 = 0, playerCard1, playerCard2;
		int[] rankMapper = new int[14];
		playerCard1 = playerCards.get(0).getRank().getRankValue();
		playerCard2 = playerCards.get(1).getRank().getRankValue();
		HandUtility.sortArrayListRankAscending(hand);

		// Maps out pairing for each rank value
		for (Card card : hand) {
			rankMapper[card.getRank().getRankValue() - 1]++;
		}

		// Loop through mapped array of ranks and store bigger counters to rank holders
		for (int i = 13; i >= 1; i--) {
			if (rankMapper[i] > containerCount1) {
				if (containerCount1 != 1) {
					containerCount2 = containerCount1;
					rankHolder2 = rankHolder1;
				}
				containerCount1 = rankMapper[i];
				rankHolder1 = i;

			} else if (rankMapper[i] > containerCount2) {
				containerCount2 = rankMapper[i];
				rankHolder2 = i;
			}
		}

		rankHolder1++;
		rankHolder2++;
		if (containerCount1 == 4) {
			handType = HandType.FOUR_OF_A_KIND;
			rankHolder2 = 0;
		} else if (containerCount2 == 2 && containerCount1 == 3) {
			handType = HandType.FULL_HOUSE;
		} else if (containerCount1 == 3) {
			handType = HandType.THREE_OF_A_KIND;
			rankHolder2 = 0;
		} else if (containerCount1 == 2 && containerCount2 == 2) {
			handType = HandType.TWO_PAIR;
		} else if (containerCount1 == 2) {
			rankHolder2 = 0;
			handType = HandType.ONE_PAIR;
		}

		return (rankHolder1 == playerCard1 || rankHolder1 == playerCard2 || rankHolder2 == playerCard1
				|| rankHolder2 == playerCard2) ? handType : HandType.HIGH_CARD;
	}
}
